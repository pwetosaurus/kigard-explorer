# Kigard Explorer

Script Grease Monkey / Tamper Monkey pour faciliter l’exploration dans le jeu web kigard (https://www.kigard.fr)

# Installation

1. Installez Grease Monkey ou Tamper Monkey dans votre navigateur.
2. Créez un nouveau Script, et copiez le contenu de userScript.js dedans.
3. Allez dans la page d’exploration (Profil > Exploration) une fois authentifié sur le site les terres de Kigard.

# Licence

 DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                    Version 2, December 2004 

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.
