// ==UserScript==
// @name     Kigard Explorer
// @author   Paul Playe
// @version  1.2.1
// @grant    none
// @include  https://www.kigard.fr/index.php?p=exploration
// ==/UserScript==

const fauna = [
  {
    name: "Lapin",
    active: true,
    biomes: [0]
  },
  {
    name: "Sanglier",
    active: true,
    biomes: [0]
  },
  {
    name: "Goblours",
    active: true,
    rank: 1,
    biomes: [0]
  },
  {
    name: "Gobelin",
    active: true,
    rank: 1,
    biomes: [0]
  },
  {
    name: "Shaman gobelin",
    active: true,
    rank: 1,
    biomes: [0]
  },
  {
    name: "Goupil",
    active: true,
    biomes: [0]
  },
  {
    name: "Ogre",
    active: true,
    rank: 2,
    biomes: [1]
  },
  {
    name: "Chevalier errant",
    active: false,
    biomes: [1]
  },
  {
    name: "",
    active: false
  },
  {
    name: "Lapin rose",
    active: false,
    biomes: [0]
  },
  {
    name: "Bandit",
    active: true,
    rank: 2,
    biomes: [0]
  },
  {
    name: "Chef bandit",
    active: true,
    rank: 2,
    biomes: [0]
  },
  {
    name: "Troll",
    active: true,
    rank: 2,
    biomes: [1]
  },
  {
    name: "Serpent géant",
    active: true,
    biomes: [2]
  },
  {
    name: "Troll guérisseur",
    active: true,
    rank: 2,
    biomes: [1]
  },
  {
    name: "Golem de pierre",
    active: true,
    rank: 2,
    biomes: [9, 10]
  },
  {
    name: "Cheval",
    active: true,
    biomes: [0]
  },
  {
    name: "Démon des sables",
    active: true,
    rank: 3,
    biomes: [2]
  },
  {
    name: "Squelette",
    active: true,
    biomes: [9]
  },
  {
    name: "Abomination",
    active: true,
    rank: 2,
    biomes: [0]
  },
  {
    name: "Yéti",
    active: true,
    rank: 3,
    biomes: [6]
  },
  {
    name: "Tarentule géante",
    active: true,
    biomes: [2]
  },
  {
    name: "Lutin",
    active: true,
    rank: 2,
    biomes: [0]
  },
  {
    name: "Cocotte",
    active: true,
    biomes: [0]
  },
  {
    name: "Morveux",
    active: true,
    rank: 2,
    biomes: [0]
  },
  {
    name: "Grenouille",
    active: true,
    biomes: [0, 3]
  },
  {
    name: "Grenouille rose",
    active: true,
    biomes: [0, 3]
  },
  {
    name: "Bandit barbare",
    active: true,
    rank: 2,
    biomes: [0]
  },
  {
    name: "Tréant",
    active: true,
    biomes: [9]
  },
  {
    name: "Fée",
    active: true,
    biomes: [0, 3]
  },
  {
    name: "Crabe géant",
    active: true,
    biomes: [7]
  },
  {
    name: "Paguro géant",
    active: true,
    biomes: [7]
  },
  {
    name: "",
    active: false
  },
  {
    name: "Centaure",
    active: true,
    rank: 2,
    biomes: [0]
  },
  {
    name: "Oublié",
    active: true,
    rank: 2,
    biomes: [2]
  },
  {
    name: "Prince oublié",
    active: true,
    rank: 3,
    biomes: [2]
  },
  {
    name: "Mulet",
    active: true,
    biomes: [0]
  },
  {
    name: "Âme en peine",
    active: true,
    rank: 3,
    biomes: [8]
  },
  {
    name: "Âme en souffrance",
    active: true,
    rank: 4,
    biomes: [8]
  },
  {
    name: "",
    active: false
  },
  {
    name: "Tentacule",
    active: true,
    biomes: [8]
  },
  {
    name: "",
    active: false
  },
  {
    name: "",
    active: false
  },
  {
    name: "",
    active: false
  },
  {
    name: "",
    active: false
  },
  {
    name: "",
    active: false
  },
  {
    name: "[PNJ] Albine",
    active: true,
    rank: 4,
    biomes: [0]
  },
  {
    name: "[PNJ] Sam le borgne",
    active: true,
    rank: 4,
    biomes: [0, 2]
  },
  {
    name: "[PNJ] Esteban",
    active: true,
    rank: 4,
    biomes: [3]
  },
  {
    name: "[PNJ] Hipolito",
    active: true,
    rank: 4,
    biomes: [2]
  },
  {
    name: "",
    active: false
  },
  {
    name: "[PNJ] Ben le vieux",
    active: true,
    rank: 4,
    biomes: [0]
  },
  {
    name: "",
    active: false
  },
  {
    name: "",
    active: false
  },
  {
    name: "",
    active: false
  },
  {
    name: "",
    active: false
  },
  {
    name: "",
    active: false
  },
  {
    name: "",
    active: false
  },
  {
    name: "",
    active: false
  },
  {
    name: "Boule d’orage",
    active: true,
    rank: 2,
    biomes: [0, 10]
  },
  {
    name: "Fée givrée",
    active: true,
    rank: 2,
    biomes: [5, 6, 11]
  },
  {
    name: "Loup",
    active: true,
    biomes: [1]
  },
  {
    name: "Élémentaire de glace",
    active: true,
    biomes: [5, 6, 9]
  },
  {
    name: "Araignée d’eau",
    active: true,
    biomes: [3]
  },
  {
    name: "Ver des sables",
    active: true,
    biomes: [2]
  },
  {
    name: "[PNJ] Brice",
    active: true,
    rank: 4,
    biomes: [3]
  },
  {
    name: "[PNJ] Venancio",
    active: true,
    rank: 4,
    biomes: [0]
  },
  {
    name: "[PNJ] Tinuviel",
    active: true,
    rank: 4,
    biomes: [0]
  },
  {
    name: "Fée nécrosée",
    active: false,
    biomes: [0]
  },
  {
    name: "Rocqueteux",
    active: true,
    biomes: [1]
  },
  {
    name: "Oiseau migrateur",
    active: false
  },
  {
    name: "Fantôme",
    active: true,
    biomes: [9]
  },
  {
    name: "Gobelin rose",
    active: true,
    rank: 1,
    biomes: [0]
  },
  {
    name: "",
    active: false
  },
  {
    name: "Sorcière hanteuse",
    active: true,
    rank: 2,
    biomes: [0]
  },
  {
    name: "Hanteur",
    active: true,
    rank: 2,
    biomes: [0]
  },
  {
    name: "Goule",
    active: true,
    biomes: [9]
  }
];

const wonders = [
  {
    name: "Un arbre doré",
    id: 68,
    position: {
      x: 0,
      y: 0
    }
  },
  {
    name: "La gardienne incandescente",
    id: 96,
    position: {
      x: 3,
      y: -89
    }
  },
  {
    name: "Un crâne de pierre",
    id: 55,
    position: {
      x: 76,
      y: -45
    }
  },
  {
    name: "Une statue d'obsidienne",
    id: 113,
    position: {
      x: -5,
      y: -60
    }
  },
  {
    name: "Une tour",
    id: 79,
    position: {
      x: 43,
      y: 42
    }
  },
  {
    name: "Un coquillage doré",
    id: 92,
    position: {
      x: -43,
      y: -84
    }
  },
  {
    name: "Une statue crapaud",
    id: 112,
    position: {
      x: -80,
      y: -10
    }
  },
  {
    name: "La gardienne de Kigard",
    id: 49,
    position: {
      x: -5,
      y: -20
    }
  },
  {
    name: "Un totem immergé",
    id: 104,
    position: {
      x: -20,
      y: 20
    }
  },
  {
    name: "Un cromlech",
    id: 67,
    position: {
      x: -68,
      y: 65
    }
  },
  {
    name: "Un cristal étincelant",
    id: 101,
    position: {
      x: 5,
      y: 95
    }
  },
  {
    name: "La gardienne de glace",
    id: 95,
    position: {
      x: 45,
      y: 59
    }
  },
  {
    name: "Une ruine",
    id: 54,
    position: {
      x: -32,
      y: 5
    }
  }
];

var biomes = [
  {
    name: 'Les Plaines de Kigard',
    icon: 'https://www.kigard.fr/images/vue/zone/plaine.gif'
  },
  {
    name: 'Les Crocs Blancs',
    icon: 'https://www.kigard.fr/images/vue/zone/montagne.gif'
  },
  {
    name: 'Le Désert de l\'Oubli',
    icon: 'https://www.kigard.fr/images/vue/zone/desert.gif'
  },
  {
    name: 'Le Marais Enchanté',
    icon: 'https://www.kigard.fr/images/vue/zone/prairie.gif'
  },
  {
    name: 'La Nécropole',
    icon: 'https://www.kigard.fr/images/vue/zone/necropole.gif'
  },
  {
    name: 'L\'Île Glaçons',
    icon: 'https://www.kigard.fr/images/vue/zone/montagne.gif'
  },
  {
    name: 'L\'Île aux Yétis',
    icon: 'https://www.kigard.fr/images/vue/zone/montagne.gif'
  },
  {
    name: 'L\'Île aux Crabes',
    icon: 'https://www.kigard.fr/images/vue/zone/desert.gif'
  },
  {
    name: 'L\'Île en Peine',
    icon: 'https://www.kigard.fr/images/vue/zone/necropole.gif'
  },
  {
    name: 'Invocation',
    icon: 'https://www.kigard.fr/images/metiers/apprenti.gif'
  },
  {
    name: 'Habitation',
    icon: 'https://www.kigard.fr/images/vue/lieu/108.gif'
  },
  {
    name: 'Clairière de la gardienne de glace',
    icon: 'https://www.kigard.fr/images/vue/lieu/95.gif'
  }
];

fauna.forEach((item, increment) => {
	item.id = increment + 1;
});

const existentFauna = fauna.filter(value => value.active);

const linkify = name => {
  let pageName = '';
  
  switch (name) {
    case "Grenouille rose":
      pageName = 'grenouille';
      break;
    case "Prince oublié":
      pageName = 'prince_oublie';
      break;
    case "Sorcière hanteuse":
      pageName = 'sorciere_hanteuse';
      break;
    case "Élémentaire de glace":
      pageName = 'elementaire_de_glace';
      break;
    default:
      pageName = name.toLowerCase().replace(/[éêè]/g, 'e').replace(/(\s|\[pnj\]|’)/g, '').replace(/ô/g, 'o');
      break;
  }
  
  return `https://wiki.kigard.fr/bestiaire/${pageName}`;
}

const seenItems = selector => {
  const ids = [];
  
  document.querySelectorAll(selector).forEach(item => {
    ids.push(item.src.match(/[0-9]{1,}/g)[0]);
  });
  
  return ids;
};

const biomesList = biomesList => {
  let html = ``;
  
  biomesList.forEach(id => {
    html += `<img src="${biomes[id].icon}" title="${biomes[id].name}" />`;
  });
  
  return html;
}

const seenFauna = seenItems('#bloc blockquote .monstre');
const seenWonders = seenItems('#bloc blockquote .lieu');

// change fauna title
const titleFauna = document.querySelector('#bloc blockquote + h3');
titleFauna.append(` (${seenFauna.length}/${existentFauna.length})`);
titleFauna.style.fontSize = "1.3em";

// change wonders title
const titleWonders = document.querySelector('#bloc h3');
titleWonders.append(` (${seenWonders.length}/${wonders.length})`);
titleWonders.style.fontSize = "1.3em";

// Find missing monsters in the list
const missingFauna = existentFauna.filter(item => {
  return seenFauna.indexOf(item.id.toString()) == -1;
});

const missingWonders = wonders.filter(item => {
  return seenWonders.indexOf(item.id.toString()) == -1;
});

const container = document.getElementById('bloc');
const missingFaunaTitle = document.createElement('h3');
const missingWondersTitle = document.createElement('h3');
const missingFaunaBlock = document.createElement('blockquote');
const missingWondersBlock = document.createElement('blockquote');

// add a new block for not yet seen fauna title
missingFaunaTitle.style.fontSize = "1.3em";
missingWondersTitle.style.fontSize = "1.3em";
missingFaunaTitle.innerText = `Animaux, Monstres et PNJ manquants (${missingFauna.length})`;
missingWondersTitle.innerText = `Monuments manquants (${missingWonders.length})`;

const addRank = rank => {
  let rankHTML = '';
  
  for(let i = 0; i < rank; i++) {
    rankHTML += ' <img src="https://www.kigard.fr/images/interface/etoile-reputation.gif" />';
  }
  
  return rankHTML;
}

// add a new block for missing monsters list
missingFauna.forEach(item => {
	missingFaunaBlock.innerHTML += `<img src="https://www.kigard.fr/images/vue/monstre/${item.id}.gif" /> <strong><a href="${linkify(item.name)}" target="_blank">${item.name}</a></strong>${addRank(item.rank)} ${biomesList(item.biomes)}<br />`;
});
missingWonders.forEach(item => {
  missingWondersBlock.innerHTML += `<img src="https://www.kigard.fr/images/vue/lieu/${item.id}.gif" /> <strong>${item.name}</strong> <img src="https://www.kigard.fr/images/interface/position.gif" />X:${item.position.x} | Y:${item.position.y}<br />`;
});

if(missingWonders.length) {
  container.append(missingWondersTitle);
  container.append(missingWondersBlock);
}

if(missingFauna.length) {
  container.append(missingFaunaTitle);
  container.append(missingFaunaBlock);
}
